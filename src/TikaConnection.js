class TikaConnection {
    constructor(hostname, port) {
        this.hostname = hostname;
        this.port = port;
    }

    static createFromEnv() {
        if (typeof process.env.TIKA_HOST === 'undefined') {
            console.warn('TIKA_HOST is undefined');
        }

        if (typeof process.env.TIKA_PORT === 'undefined') {
            console.warn('TIKA_PORT is undefined');
        }

        return new TikaConnection(process.env.TIKA_HOST, process.env.TIKA_PORT);
    }
}

module.exports = TikaConnection;
