const Tika = require('./Tika')
const TikaConnection = require('./TikaConnection')

module.exports = {
    Tika,
    TikaConnection
}