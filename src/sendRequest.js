var http = require('http');

function sendRequest(options, dataBuffer) {
    return new Promise((resolve, reject) => {
        var req = http.request(options, function (res) {
            var chunks = [];

            res.on("data", function (chunk) {
                chunks.push(chunk);
            });

            res.on("end", function (chunk) {
                var body = Buffer.concat(chunks);
                resolve({
                    statusCode: res.statusCode,
                    body: body.toString()
                });
            });


            res.on("error", function (error) {
                reject(error);
            });
        });

        req.on('error', (e) => {
            reject(e);
        })

        if (typeof dataBuffer !== 'undefined') {
            req.write(dataBuffer);
        }

        req.end();
    })
}

module.exports = sendRequest;