const sendRequest = require('./sendRequest')

const is2XXErrorCode = (code) => code >= 200 && code < 300


class Tika {
    constructor(tikaConnection) {
        this.tikaConnection = tikaConnection;
    }

    async _put(url, headers, data) {
        return await sendRequest({
            'method': 'PUT',
            'hostname': this.tikaConnection.hostname,
            'port': this.tikaConnection.port,
            'path': url,
            'headers': headers
        }, data);
    }

    async _get(url, headers) {
        return await sendRequest({
            'method': 'GET',
            'hostname': this.tikaConnection.hostname,
            'port': this.tikaConnection.port,
            'path': url,
            'headers': headers
        });
    }

    async tika(dataBuffer) {
        try {
            const result = await this._put(
                '/tika',
                {
                    'Accept': 'application/json',
                    'Content-Type': 'application/pdf'
                },
                dataBuffer
            );

            if (!is2XXErrorCode(result.statusCode)) {
                throw `Tika replied with status code ${result.statusCode}.\n${result.body}`;
            }

            return result.body;
        } catch (e) {
            throw new Error(e);
        }
    }

    async version() {
        try {
            const result = await this._get('/version', {});

            if (!is2XXErrorCode(result.statusCode)) {
                throw `Tika replied with status code ${result.statusCode}.\n${result.body}`;
            }

            return result.body;
        } catch (e) {
            throw new Error(e);
        }
    }

    async parsers() {
        try {
            const result = await this._get(
                '/parsers',
                {
                    'Accept': 'application/json'
                }
            );

            if (!is2XXErrorCode(result.statusCode)) {
                throw `Tika replied with status code ${result.statusCode}.\n${result.body}`;
            }

            return result.body;
        } catch (e) {
            throw new Error(e);
        }
    }
}

module.exports = Tika;