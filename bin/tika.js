#!/usr/bin/env node

const { Tika, TikaConnection } = require('../src/index');

const fs = require('fs').promises;

const Yargs = require('yargs/yargs');
const argv = Yargs(process.argv.slice(2))
    .command('tika [file]', 'Send a file to be parsed with Tika', (yargs) => {
        yargs.positional('file', {
            describe: 'A path to the file',
            type: 'string'
        })
    })
    .demandCommand(1)
    .command('version', 'Gets the Tika version.', (yargs) => {})
    .command('parsers', 'Gets parsers from Tika.', (yargs) => {})
    .describe('tika-host', 'The hostname for the tika server. If not provided, uses TIKA_HOST')
    .describe('tika-port', 'The port for the tika server. If not provided, uses TIKA_PORT.')
    .default({
        'tika-host': process.env.TIKA_HOST ?? 'localhost',
        'tika-port': process.env.TIKA_PORT ?? '9998'
    })
    .argv;

const tikaConnection = new TikaConnection(argv.tikaHost, argv.tikaPort);
const tika = new Tika(tikaConnection);

const fn = async () => {
    try {
        switch (argv._[0]) {
            case "tika":
                const data = await fs.readFile(argv.file);
                console.log(await tika.tika(data));
                break;
            case "version":
                console.log(await tika.version());
                break;
            case "parsers":
                console.log(await tika.parsers());
                break;
            default:
                console.error(`Unknown command: ${argv._[0]}`);
                break;
        }
    } catch (e) {
        console.log(e.message);
    }
}

fn();
